import React, { Component } from "react";
import SideBar from "./sideBar";
import Results from "./results";
import axios from "axios";

class Client extends Component {
  state = {
    flights: [],
    searchRequest: "Search to find flights",
    flightResults: [],
    isFlightReturning: false,
  };

  handleSearchRequest = (flight) => {
    let originFlights = this.state.flights;
    let returnFlights = [];
    this.setState({
      isFlightReturning: flight.returnDate !== "" ? true : false,
    });

    if (flight.returnDate !== "") {
      returnFlights = originFlights;
      this.setState({ isFlightReturning: true });
    }

    if (originFlights === null) return;

    originFlights = originFlights.filter(
      (f) =>
        f.origin === flight.origin &&
        f.destination === flight.destination &&
        f.departDate === flight.departDate &&
        f.cost >= flight.minPrice &&
        f.cost <= flight.maxPrice
    );

    if (returnFlights !== []) {
      returnFlights = returnFlights.filter(
        (f) =>
          f.origin === flight.destination &&
          f.destination === flight.origin &&
          f.departDate === flight.returnDate &&
          f.cost >= flight.minPrice &&
          f.cost <= flight.maxPrice
      );
    }

    for (let index = 0; index < returnFlights.length; index++) {
      if (
        returnFlights[index].departDate === flight.departDate &&
        returnFlights[index].departTime >= originFlights.arriveTime
      ) {
        returnFlights.splice(index);
      }
    }
    let results = [];
    if (flight.departDate === flight.returnDate) {
      for (let x = 0; x < originFlights.length; x++) {
        for (let y = 0; y < returnFlights.length; y++) {
          if (originFlights[x].arriveTime < returnFlights[y].departTime) {
            const result = [originFlights[x], returnFlights[y]];
            results.push(result);
          }
        }
      }
    } else {
      for (let index = 0; index < originFlights.length; index++) {
        const result = [originFlights[index], returnFlights[index]];
        results.push(result);
      }
    }

    let search =
      flight.returnDate !== ""
        ? flight.origin + " > " + flight.destination + " > " + flight.origin
        : flight.origin + " > " + flight.destination;

    this.setState({
      searchRequest: search,
      flightResults: results,
    });
  };

  componentDidMount() {
    axios
      .get("./data/flights.json")
      .then((res) => {
        this.setState({ flights: res.data });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  render() {
    return (
      <div className="App">
        <div className="container">
          <header className="p-2">
            <h1 className="h2">Flight Search Engine</h1>
          </header>
          <div className="row">
            <SideBar onSearch={this.handleSearchRequest} />
            <main role="main" className="col-md-9 ml-sm-auto col-lg-8 ">
              <Results
                searchRequest={this.state.searchRequest}
                flightResults={this.state.flightResults}
                isReturning={this.state.isFlightReturning}
              />
            </main>
          </div>
        </div>
      </div>
    );
  }
}

export default Client;
