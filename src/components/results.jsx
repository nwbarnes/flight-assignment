import React, { Component } from "react";
import ResultListing from "./resultListing";

class Results extends Component {
  componentDidUpdate() {}

  handleResults = () => {
    let flights = this.props.flightResults;
    console.log(flights);
    if (flights == null) return;

    if (this.props.isReturning) {
      flights = flights.filter((f) => f[1] != null);
    }

    return flights.map((flight) => (
      <ResultListing key={flight[0].slug} flight={flight} />
    ));
  };

  render() {
    return (
      <div className="container pt-2 pb-2 border ">
        <h1>{this.props.searchRequest}</h1>

        <div className="overflow-auto">{this.handleResults()}</div>
      </div>
    );
  }
}

export default Results;
