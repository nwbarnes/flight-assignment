import React, { useState } from "react";

function SideBar(props) {
  const [currentPage, setPage] = useState(0);

  // Flight Search States
  const [origin, setOrigin] = useState("");
  const [destination, setDestination] = useState("");
  const [departDate, setDepartDate] = useState("");
  const [returnDate, setReturnDate] = useState("");
  // Search Refine States
  const [minPrice, setMinPrice] = useState(0);
  const [maxPrice, setMaxPrice] = useState(1000000);

  const handlePageSubmit = (event) => {
    event.preventDefault();
  };

  const handleSearchSubmit = (event) => {
    event.preventDefault();

    let flight = {
      origin,
      destination,
      departDate,
      returnDate,
      minPrice,
      maxPrice,
      isReturning: 0,
    };

    console.log(flight);
    props.onSearch(flight);
  };

  return (
    <nav className=" col-md-4 bg-light sidebar ">
      <div className=" sidebar-sticky ">
        <form
          className="d-flex justify-content-center"
          onSubmit={handlePageSubmit}
        >
          <button
            className={
              currentPage === 0
                ? "btn btn-secondary "
                : "btn btn-secondary disabled"
            }
            onClick={() => setPage(0)}
          >
            One way
          </button>
          <button
            className={
              currentPage === 0
                ? "btn btn-secondary disabled"
                : "btn btn-secondary "
            }
            onClick={() => {
              setPage(1);
              setReturnDate("");
            }}
          >
            Return
          </button>
        </form>
        <form
          className="needs-validation p-2 bg-secondary rounded"
          onSubmit={handleSearchSubmit}
        >
          <input
            type="text"
            className="form-control"
            placeholder="enter origin"
            onChange={(e) => setOrigin(e.target.value)}
            required
          />
          <input
            type="text"
            className="form-control"
            placeholder="enter destination"
            onChange={(e) => setDestination(e.target.value)}
            required
          />
          <div>
            <input
              type="text"
              onFocus={(e) => (e.target.type = "date")}
              onBlur={(e) =>
                e.target.value === "" ? (e.target.type = "text") : ""
              }
              className="form-control"
              placeholder="departure date"
              onChange={(e) => setDepartDate(e.target.value)}
              required
            />
            <div class="invalid-feedback">Looks good!</div>
          </div>
          {currentPage === 1 && (
            <input
              type="text"
              onFocus={(e) => (e.target.type = "date")}
              onBlur={(e) =>
                e.target.value === "" ? (e.target.type = "text") : ""
              }
              className="form-control"
              placeholder="return date"
              onChange={(e) => setReturnDate(e.target.value)}
            />
          )}

          <button type="submit" className="btn btn-primary mt-1">
            Search
          </button>
        </form>
        <form
          className="p-2 bg-secondary rounded mt-2 text-light"
          onSubmit={handleSearchSubmit}
        >
          <h6 className="text-light">Refine Search Results</h6>
          <label>Min Price: ${minPrice}</label>
          <input
            type="range"
            min="0"
            max="1000000"
            defaultValue="0"
            onChange={(e) => setMinPrice(e.target.value)}
            className="form-control-range"
          />
          <label>Max Price: ${maxPrice}</label>
          <input
            type="range"
            min="0"
            max="1000000"
            defaultValue="1000000"
            onChange={(e) => setMaxPrice(e.target.value)}
            className="form-control-range"
          />
        </form>
      </div>
    </nav>
  );
}

export default SideBar;
