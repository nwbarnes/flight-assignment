import React from "react";

function ResultListing({ flight: f }) {
  return (
    // origin flight
    // if retuning return flight
    <div className="pt-2 pb-2 border container mt-2 mb-2">
      <h2>{f[1] != null ? f[0].cost + f[1].cost : f[0].cost}</h2>
      <div className="row">
        <div className="col-xs-6 col-md-4">
          <small>{f[0].slug}</small>
          <h5>
            {f[0].origin} > {f[0].destination}
          </h5>
          <h6>Depart: {f[0].departTime}</h6>
          <h6>Arrive: {f[0].arriveTime}</h6>
        </div>
        <div className="col-xs-6 col-md-4">
          {f[1] != null && (
            <div>
              <small>{f[1].slug}</small>
              <h5>
                {f[1].origin} > {f[1].destination}
              </h5>
              <h6>Depart: {f[1].departTime}</h6>
              <h6>Arrive: {f[1].arriveTime}</h6>
            </div>
          )}
        </div>
        <div align="center" className="col-xs-6 col-md-4">
          <img src="https://via.placeholder.com/100x75" alt="airline" />
          <br />
          <button type="submit" className="btn btn-primary mt-1">
            book this flight
          </button>
        </div>
      </div>
    </div>
  );
}

export default ResultListing;
